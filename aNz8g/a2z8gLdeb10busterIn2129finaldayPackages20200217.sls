# Salt suitable list of packages to install generated by
# ./base2manual.py 'sls' 'manual' 'all'
## "Debian GNU/Linux 10 (buster)" ; "Debian GNU/Linux" ; buster
# 20200217T205257 - Total of 2129 packages detected
# 20200217T205257 - Total of 173 'manual' packages will be listed below
base2manualA_install:
  pkg.installed:
    - pkgs:
      - adduser
      - apt
      - apt-listchanges
      - apt-transport-https
      - apt-utils
      - aptitude

base2manualB_install:
  pkg.installed:
    - pkgs:
      - base-files
      - base-passwd
      - bash
      - bash-completion
      - bind9-host
      - bsdmainutils
      - bsdutils
      - busybox

base2manualC_install:
  pkg.installed:
    - pkgs:
      - ca-certificates
      - chromium
      - chromium-driver
      - console-setup
      - coreutils
      - cpio
      - cron
      - cryptsetup-bin
      - cryptsetup-run
      - curl

base2manualD_install:
  pkg.installed:
    - pkgs:
      - dash
      - dbus
      - debconf
      - debconf-i18n
      - debian-archive-keyring
      - debian-faq
      - debianutils
      - debootstrap
      - default-mysql-server
      - diffutils
      - discover
      - dmidecode
      - dmsetup
      - doc-debian
      - docker-doc
      - docker.io
      - dpkg

base2manualE_install:
  pkg.installed:
    - pkgs:
      - e2fsprogs
      - ed
      - emma

base2manualF_install:
  pkg.installed:
    - pkgs:
      - fdisk
      - findutils
      - firmware-amd-graphics
      - firmware-iwlwifi
      - fsarchiver

base2manualG_install:
  pkg.installed:
    - pkgs:
      - gdbm-l10n
      - geany
      - geany-plugin-addons
      - geany-plugin-lineoperations
      - geany-plugin-shiftcolumn
      - gettext-base
      - gimp
      - gimp-help-en
      - gkrellm
      - glances
      - google-cloud-sdk
      - gpgv
      - grep
      - groff-base
      - grub-common
      - grub-efi-amd64
      - gzip

base2manualH_install:
  pkg.installed:
    - pkgs:
      - hdparm
      - hostname

base2manualI_install:
  pkg.installed:
    - pkgs:
      - icli
      - ifupdown
      - init
      - init-system-helpers
      - initramfs-tools
      - installation-report
      - iproute2
      - iptables
      - iputils-ping
      - isc-dhcp-client
      - isc-dhcp-common

base2manualJ_install:
  pkg.installed:
    - pkgs:
      - jq

base2manualK_install:
  pkg.installed:
    - pkgs:
      - keyboard-configuration
      - keyutils
      - kmod
      - krb5-locales
      - kubectl

base2manualL_install:
  pkg.installed:
    - pkgs:
      - laptop-detect
      - less
      - libaudit-common
      - libc-bin
      - libcap2-bin
      - libcpufreq0
      - libdns-export1104
      - liblocale-gettext-perl
      - liblockfile-bin
      - libpam-modules-bin
      - libpam-runtime
      - libsemanage-common
      - libtext-charwidth-perl
      - libtext-iconv-perl
      - libtext-wrapi18n-perl
      - linux-image-amd64
      - locales
      - login
      - logrotate
      - lsb-base
      - lsof

base2manualM_install:
  pkg.installed:
    - pkgs:
      - man-db
      - manpages
      - mariadb-client-10.3
      - mawk
      - mercurial
      - mount
      - mysql-workbench-community

base2manualN_install:
  pkg.installed:
    - pkgs:
      - nano
      - ncurses-base
      - ncurses-bin
      - ncurses-term
      - net-tools
      - netbase
      - netcat-traditional

base2manualO_install:
  pkg.installed:
    - pkgs:
      - openssh-client

base2manualP_install:
  pkg.installed:
    - pkgs:
      - passwd
      - pciutils
      - perl
      - perl-base
      - perl-modules-5.28
      - procps
      - python
      - python-egenix-mxdatetime
      - python-minimal
      - python-mysqldb
      - python2.7
      - python3-reportbug

base2manualR_install:
  pkg.installed:
    - pkgs:
      - readline-common
      - reportbug
      - rsyslog

base2manualS_install:
  pkg.installed:
    - pkgs:
      - sed
      - sensible-utils
      - sysstat
      - systemd
      - systemd-sysv
      - sysvinit-utils

base2manualT_install:
  pkg.installed:
    - pkgs:
      - tar
      - task-british-desktop
      - task-desktop
      - task-english
      - task-gnome-desktop
      - task-laptop
      - task-print-server
      - task-web-server
      - task-xfce-desktop
      - tasksel
      - tasksel-data
      - telnet
      - traceroute
      - tzdata

base2manualU_install:
  pkg.installed:
    - pkgs:
      - ucf
      - udev
      - usbutils
      - util-linux

base2manualV_install:
  pkg.installed:
    - pkgs:
      - vim-common
      - vim-tiny
      - vlc

base2manualW_install:
  pkg.installed:
    - pkgs:
      - wamerican
      - wget
      - whiptail

base2manualX_install:
  pkg.installed:
    - pkgs:
      - xclip
      - xfce4-terminal
      - xscreensaver
      - xscreensaver-gl
      - xxd

base2manualZ_install:
  pkg.installed:
    - pkgs:
      - zile
      - zip

