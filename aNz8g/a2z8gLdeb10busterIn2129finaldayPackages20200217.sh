#!/bin/dash
## "Debian GNU/Linux 10 (buster)" ; "Debian GNU/Linux" ; buster
# 20200217T205249 - Total of 173 packages will be listed below
apt-get install adduser apt apt-listchanges apt-transport-https apt-utils aptitude;
apt-get install base-files base-passwd bash bash-completion bind9-host bsdmainutils;
apt-get install bsdutils busybox ca-certificates chromium chromium-driver;
apt-get install console-setup coreutils cpio cron cryptsetup-bin cryptsetup-run curl;
apt-get install dash dbus debconf debconf-i18n debian-archive-keyring debian-faq;
apt-get install debianutils debootstrap default-mysql-server diffutils discover;
apt-get install dmidecode dmsetup doc-debian docker-doc docker.io dpkg e2fsprogs ed emma;
apt-get install fdisk findutils firmware-amd-graphics firmware-iwlwifi fsarchiver;
apt-get install gdbm-l10n geany geany-plugin-addons geany-plugin-lineoperations;
apt-get install geany-plugin-shiftcolumn gettext-base gimp gimp-help-en gkrellm;
apt-get install glances google-cloud-sdk gpgv grep groff-base grub-common;
apt-get install grub-efi-amd64 gzip hdparm hostname icli ifupdown init;
apt-get install init-system-helpers initramfs-tools installation-report iproute2;
apt-get install iptables iputils-ping isc-dhcp-client isc-dhcp-common jq;
apt-get install keyboard-configuration keyutils kmod krb5-locales kubectl;
apt-get install laptop-detect less libaudit-common libc-bin libcap2-bin libcpufreq0;
apt-get install libdns-export1104 liblocale-gettext-perl liblockfile-bin;
apt-get install libpam-modules-bin libpam-runtime libsemanage-common;
apt-get install libtext-charwidth-perl libtext-iconv-perl libtext-wrapi18n-perl;
apt-get install linux-image-amd64 locales login logrotate lsb-base lsof man-db manpages;
apt-get install mariadb-client-10.3 mawk mercurial mount mysql-workbench-community;
apt-get install nano ncurses-base ncurses-bin ncurses-term net-tools netbase;
apt-get install netcat-traditional openssh-client passwd pciutils perl perl-base;
apt-get install perl-modules-5.28 procps python python-egenix-mxdatetime;
apt-get install python-minimal python-mysqldb python2.7 python3-reportbug;
apt-get install readline-common reportbug rsyslog sed sensible-utils sysstat systemd;
apt-get install systemd-sysv sysvinit-utils tar task-british-desktop task-desktop;
apt-get install task-english task-gnome-desktop task-laptop task-print-server;
apt-get install task-web-server task-xfce-desktop tasksel tasksel-data telnet;
apt-get install traceroute tzdata ucf udev usbutils util-linux vim-common vim-tiny vlc;
apt-get install wamerican wget whiptail xclip xfce4-terminal xscreensaver;
apt-get install xscreensaver-gl xxd zile zip;
